﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _TeamDefins;

public class AutomaticScreenSegmentation : MonoBehaviour {
	// プレイヤーのデータインスタンスを保持
	private PlayerDataManager m_PlayerdataManager;
	// カメラのインスタンス
	private Camera m_camera;

	// Use this for initialization
	void Start () {
		// データインスタンスをもらう
		m_PlayerdataManager = this.GetComponent<PlayerDataManager>();
		m_camera = this.transform.Find("Camera").GetComponent<Camera>();

		// カメラの自動分割
		AutomaticSegmentation();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 画面の自動分割
	void AutomaticSegmentation() {
		// 自分のIDでカメラの位置を変える
		float x = (float)m_PlayerdataManager.m_id * 0.25f;

		// カメラのサイズを設定
		m_camera.rect = new Rect(x, 0, 0.25f, 1);
	}
}
