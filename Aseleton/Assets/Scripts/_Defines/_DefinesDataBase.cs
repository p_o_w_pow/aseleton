﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// チーム全体で使用したい定数データの定義スペース
namespace _TeamDefins{
	// プレイヤーIDの管理
	public enum PlayerID {
		PLAYER_RED,
		PLAYER_GREEN,
		PLAYER_BLUE,
		PLAYER_YELLOW,
		ALL	// 全プレイヤー人数
	}


	// スタート位置を決めるための構造体
	public class StartPosition {
		public float fallBack;  // 後ろに下がる量
		public float division;  // 隣のやつとの幅

		// コンストラクタ
		StartPosition() {
			// ０初期化
			fallBack = 0;
			division = 0;
		}
	};



}



public class _DefinesDataBase : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
