﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTape : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//当たった時
	void OnTriggerEnter(Collider other)
	{
		//プレイヤーのゴールフラグがfalseだったらゴールとしてカウント
		if(other.GetComponent<PlayerGoalCheck>().IsGoalCheckFlag()==false)
		{
			//ゴール数カウント
			other.GetComponent<PlayerGoalCheck>().AddGoalCount();
			//フラグをtrueへ(出入りを防ぐため)
			other.GetComponent<PlayerGoalCheck>().ChangeGaolFlag();
		}
	}
}
