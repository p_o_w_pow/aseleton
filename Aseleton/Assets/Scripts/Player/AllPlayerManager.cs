﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _TeamDefins;


public class AllPlayerManager : MonoBehaviour {

	public float m_fallBackValue; // 後ろに下がる基準量
	public float m_divisionValue;	// プレイヤーとプレイヤーの間の基準値


	// プレイヤーのリスト
	public List<GameObject> m_playerList;


	// Use this for initialization
	void Start() {
		for(int i = 0; i < (int)PlayerID.ALL; i++) {
			// PlayerDataManagerのインスタンスを取得
			PlayerDataManager pdm = m_playerList[i].GetComponent<PlayerDataManager>();

			// スタート位置をプレイヤーごとにずらして設定
			pdm.SetStartPosition(m_fallBackValue * (i + 1), m_divisionValue * (i + 1));

		}
		
	}


	// Update is called once per frame
	void Update() {

	}
}
