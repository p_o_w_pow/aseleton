﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _TeamDefins;

// プレイヤーの基礎的なデータを管理する
public class PlayerDataManager : MonoBehaviour {
	// プレイヤーのID
	public PlayerID m_id;

	public StartPosition m_startPosition { get; protected set; }

	// プレイヤーを見るカメラ
	private Camera m_camera;


	// Use this for initialization
	void Start () {
		// カメラのインスタンス取得
		m_camera = this.GetComponent<Camera>();

		// マテリアルの色を初期化
		PlayerCollorInit();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	// 自身のマテリアルの色を変える
	private void PlayerCollorInit() {
		// マテリアルを取得
		Renderer render = this.GetComponent<Renderer>();

		// プレイヤーの色を変える
		switch (m_id) {
		// 赤キャラ
		case PlayerID.PLAYER_RED:
			// マテリアルに赤をセット
			render.material.color = Color.red;
			break;

		// 青キャラ
		case PlayerID.PLAYER_BLUE:
			// マテリアルに青をセット
			render.material.color = Color.blue;
			break;

		// 緑キャラ
		case PlayerID.PLAYER_GREEN:
			// マテリアルに赤をセット
			render.material.color = Color.green;
			break;

		// 黃キャラ
		case PlayerID.PLAYER_YELLOW:
			// マテリアルに赤をセット
			render.material.color = Color.yellow;
			break;
		}
	}


	// スタート位置を設定する
	public void SetStartPosition(float _fallBack, float _division) {
		this.transform.position = new Vector3(_division, this.transform.position.y, _fallBack);
		Debug.Log(this.transform.position);
	}

}
