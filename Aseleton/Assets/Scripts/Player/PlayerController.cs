﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float m_speed { get; protected set; }	// 速度

	private Camera m_camera;	// カメラ
	

	// Use this for initialization
	void Start () {
		// カメラのインスタンスを取得
		this.transform.Find("PlayerCamera");

		// 各データを初期化
		m_speed = 0;


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	
	// パラメータの初期化
	private void ParameterReset() {

	}
}
