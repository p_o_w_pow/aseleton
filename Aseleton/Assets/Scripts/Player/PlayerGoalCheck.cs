﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGoalCheck : MonoBehaviour {

    private Vector3 m_pos;      //ポジション変更用
    private bool m_goalCheck;   //ゴール判定
    private GameObject m_goalObject;    //ゴールオブジェクト
	private int m_goalCount;    //ゴールした回数
	public int m_endGoalCount;	//ゲームが終わるゴール回数

    // Use this for initialization
    void Start () {
		m_goalCount = 0;

		m_pos.x = 0;
		m_pos.y = 0;
		m_pos.z = -0.1f;

		m_goalCheck = true;
    }

    // Update is called once per frame
    void Update () {

		this.transform.position += m_pos;
		//Debug.Log(this.transform.position);

		//Debug.Log(this.m_goalCheck);
		//Debug.Log(m_goalObject.name);

		Debug.Log(this.m_goalCheck);

	}

	//ゴール判定のフラグの切り替え
	public void ChangeGaolFlag()
    {
        m_goalCheck = !m_goalCheck;
    }

	//ゴール数のカウントを増やす
	public void AddGoalCount()
	{
		m_goalCount++;
	}

    public void startPos()
    {
		m_pos.x = 0;
		m_pos.y = 0;
        m_pos.z = -1;
		Debug.Log(this.transform.position);
	}

	//フラグの情報取得
	public bool IsGoalCheckFlag()
	{
		return m_goalCheck;
	}
}
